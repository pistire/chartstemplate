import matplotlib.pyplot as plt



def plot_Fx_SR(tire_model, Fz, SR, SA, IA, P):
    x = SR
    y = []
    for vertical_force in Fz:
        sub_y = []
        for slip_ratio in SR:
            sub_y.append(tire_model.Fx(vertical_force, slip_ratio))

        y.append(sub_y)

    __plot_chart__(x, y)


def plot_Fy_SA(tire_model, FZ, SA, IA, SR, P):
    x = SA
    y = []
    legend = []
    for sa in SA:
        for sr in SR:
            for fz in FZ:
                for ia in IA:
                    for p in P:
                        y.append(tire_model.Fy(Fz=fz, SA=sa, IA=ia, SR=sr, P=p))
                        legend.append("Fz: " + str(fz) + " | " + "IA: " + str(ia) + " | " + "SR: " + str(sr) + " | " + "P: " + str(p))


    __plot_chart__(x, y, legend)



def plot_Mz_SA(tire_model, Fz, SA, IA, SR, P):
    pass


def __plot_chart__(x, y, legend):
    _, ax = plt.subplots()
    for y_data, len in zip(y, legend):
        ax.plot(x[0], y_data, label=len)

    ax.legend(bbox_to_anchor=(1.04,1), loc="upper left")
    plt.show()